<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {

    public $pageType = 'ajax';

    function __construct()
    {
        parent::__construct();
    }

    function stripe_hook() {
        $this->load->helper(
            array(
                'transfers',
                'transaction'
            )
        );

        $result = array();

        // Configuration options
        $this->load->library('stripe', $this->config->item('stripeConfig'));

        // Retrieve the request's body and parse it as JSON
        $input = @file_get_contents("php://input");
        $event_json = json_decode($input);

        $eventType = isset($event_json->type) ? $event_json->type : '';
        switch ($eventType) {
            // Transfer events
            case 'transfer.paid':
            case 'transfer.failed':
            // Charge events
            case 'charge.succeeded':
            case 'charge.failed':
                $result = processTransactionFromStripeHook($event_json->data->object, $eventType);

            break; case 'balance.available':
                updateBalanceAvailable($event_json->data->object);

            break; default:
                if (!empty($eventType)) {
                    $result = array('error' => 'uncaught_event', 'errorMessage' => 'Unable to find event: ' . $eventType);
                } else {
                    show_404();
                    exit;
                }
        }

        jsonHeader();
        echo json_encode(array($eventType => $result));
        header("HTTP/1.1 200 OK");
        exit;
    }

    function getMenuItems()
    {
        $this->load->helper('nsec_account');

        jsonHeader();
        $where = $output = NULL;
        $idList = getUserAccountInfo('AccessLevels');

        $results = $this->db->query(
            "select * from access_types where AccessLevelID in ("
            . (($idList !== null) ? $idList : 1)
            . ") order by ParentId, AccessRef"
        );

        if ($results!=false && $results->num_rows() > 0) {
            foreach ( $results->result() as $row ) {
                $output[$row->AccessLevelID] = array(
                    'value' => $row->AccessPage,
                    'text' => $row->AccessName,
                    'parent' => $row->ParentId,
                    'ref' => $row->AccessRef
                );
            }
        }
        
        $output['1000'] = array('value' => '/account/logout/', 'text' => 'Logout', 'parent' => null);

        echo json_encode($output);
        die();
    }

    function mailhandler() {
        if ($this->input->post('fromForm')) {
            $this->load->library('email');

            $this->email->from($this->input->post('email'), $this->config->item('name'));

            $this->email->to($this->config->item('SiteEmail'), $this->config->item('SiteName'));

            $this->email->subject($this->config->item('SiteName') . ": New Message from " . $this->input->post('name'));

            $content = "Name: " . $this->input->post('name') . "\n";
            $content .= "Email: " . $this->input->post('email') . "\n";
            $content .= "Phone: " . $this->input->post('phone') . "\n";
            $content .= $this->input->post('message') . "\n";
            $content .= "Sent: " . date('Y-m-d H:i:s') . "\n";
            $content .= "";
            $this->email->message($content);

            $this->email->send();
        } else {
            show_404();
            exit;
        }

    }
}
