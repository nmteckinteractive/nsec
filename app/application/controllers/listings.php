<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Listings extends CI_Controller {

    public $pageType = 'listings';

    public $currentListing;

    function __construct()
    {
        parent::__construct();
        $this->load->library('userprofile');
        $this->load->library('form_validation');
        $this->load->helper('listings');
        $this->load->helper('account_helper');
    }

    function index($type='forsale', $listingid=NULL, $page=false)
    {
        $section = 'member_listings';
        if ($type == 'edit' || $type == 'delete') {
            if (is_logged_in()) {
                if ($type == 'delete') {
                    $this->db->set('DateDeleted', date('Y-m-d H:i:s'));
                    $this->db->where('ListingID', $listingid);
                    $this->db->update($section);

                    redirect('account');
                }

                $this->load->helper('listings');
                $this->db->where('ListingID', $listingid);
                
                $this->db->like('DateDeleted', '0000-00-00', 'after');
                $this->db->order_by('Price', 'desc');
                $this->db->limit(50);
                $data['listing'] = $this->db->get($section . ' t')->row();

                $data['contentPage'] = 'listing/' . $type;
                $data['metadata'] = generate_meta('account', 'Edit Listing', array('subsection'=>'edit_listing'));
            } else {
                $this->session->set_userdata('redirect_url', '/listings/edit/' . $listingid);
                redirect('login');
            }
        } else {
            $this->load->model('filters');
            $section = 'member_listings';
            $data['curpage']='listingindex';
            $data['pagetype']='listings';
            $pageTitle = 'Item Listings';

            if ($type == 'category') {
                $catId = $listingid;
                $listingid = null;
            }

            if (usertype('admin')) {
                if ($type == 'approve' && $listingid != null) {
                    $this->db->where('ListingID', $listingid);
                    $this->db->set('Approved', 1);
                    $this->db->update($section);
                }
            } elseif(!is_logged_in())
                $this->db->where('t.Approved', 1);

            
            if ($listingid != NULL) {
                $data['curpage'] = 'details';
                $data['pagetype'] = 'listing_details';
                $data['contentPage'] = 'listing_details';
                $this->db->where('ListingID', $listingid);
            } else {
                if($type == 'featured') {
                    $this->db->where('Featured', 1);
                } else {
                    $this->db->where('DateSaved >', date('Y-m-d H:i:s', strtotime('-30 days')));
                }

                $this->db->like('DateDeleted', '0000-00-00', 'after');
                
            }
            
            $this->db->select('*, (DateDeleted NOT LIKE \'0000-00-00%\') as IsDeleted');
            $this->db->order_by('Price', 'desc');
            $this->db->join('category_types cat', 'cat.categoryId=category', 'left');
            
            if($this->input->post('make'))
                $this->db->where('Make', $this->input->post('make'));
            
            if($this->input->post('model'))
                $this->db->where('Model', $this->input->post('model'));

            if(isset($catId))
                $this->db->where('shortName', $catId);
            
            if($this->input->post('yearMin'))
                $this->db->where('Year >', $this->input->post('yearMin'));
            
            if($this->input->post('yearMax'))
                $this->db->where('Year <', $this->input->post('yearMax'));
            
            if($this->input->post('priceMin'))
                $this->db->where('Price >', $this->input->post('priceMin'));
            
            if($this->input->post('priceMax'))
                $this->db->where('Price <', $this->input->post('priceMax'));
            
            if($this->input->post('make'))
                $this->session->set_userdata('searchData', $_POST);
            
            $this->db->limit(50);

            $data['listings'] = $this->db->get($section.' t');
            if ($listingid != NULL && $data['listings']->num_rows() == 1) {
                $this->currentListing = $firstRow = $data['listings']->row();
                $pageTitle = 'For Sale: ' . $firstRow->Title . ' in ' . $firstRow->Location;

                $this->db->where('ListingID', $firstRow->ListingID);
                $this->db->set('Views', $firstRow->Views + 1);
                $this->db->update($section);
            } elseif ($listingid != NULL && $data['listings']->num_rows() == 0) {
                show_404();
            }

            $data['metadata'] = generate_meta('listings', $pageTitle, array('subsection'=>$data['pagetype']));
            //echo $this->db->last_query();
                    
            $data['PageTitle'] = 'Listings Search Results';
            $data['admin_page'] = 'listingindex';
        }

        $this->load->view('users', $data);
    }
}

?>