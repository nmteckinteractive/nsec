<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    public $pageType;

    public $userType;

    public $userAccessLevels;

    public $userAccessGroups;

    public $userAccessTypes;

    public $userAccountInfo;

    public $pageBlocks;

    public $profile;

    public $Errors;

    public $memberTable = 'members';

    public $userTable = 'users';

    public $accountTable = 'accounts';

    function __construct()
    {
        parent::__construct();
        $this->load->library('simplelogin');
        $this->load->library('user');
        $this->load->library('pagination');

        $this->load->helper('nsec_account_helper');
        if (getUserAccountInfo('default_account_id') == 1) {
            //$this->output->enable_profiler(TRUE);
            $sections = array(
                'config'  => FALSE
            );

            //$this->output->set_profiler_sections($sections);
        }

        $this->_setPageBlocks();
    }

    public function preDispatch($validateLogin = true)
    {
        if (($validateLogin == true) && !is_logged_in()) {
            redirect('login');
        }

        if (is_logged_in()) {
            if ($this->pageType == 'admin' && !usertype('admin')){
                $this->logout();
            }
            $this->load->model('nsec_userprofile');
            $this->_loadUserProfile();
        }       
    }

    public function _getPageBlock($block) {
        return isset($this->pageBlocks[$block]) ? $this->pageBlocks[$block] : '';
    }

    public function _logout()
    {
        //Logout
        $this->simplelogin->logout();
        redirect('login');
    }

    /***** Private functions ****/

    private function _setPageBlocks() {
        $this->pageBlocks = getPageBlocks();
    }

    private function _updateUserPhoto()
    {
        if (strtolower(getUserAccountInfo('account_type')) == 'member') {
            $this->profile->updateUserPhoto('members', 'id');
        }

        $this->_loadUserProfile();
    }

    private function _loadUserProfile()
    {
        $this->profile = new $this->nsec_userprofile($this->session->userdata('user_id'));
    }

    private function _updateUserProfile()
    {
        if (strtolower(getUserAccountInfo('account_type')) == 'member') {
            if ($this->input->post('company'))
                $_POST['account_name'] = $this->input->post('company');

            if ($this->input->post('email'))
                $_POST['email_address'] = $this->input->post('email');

            if ($this->input->post('first_name'))
                $_POST['contact_name'] = $this->input->post('first_name') . ' ' . $this->input->post('last_name');

            if ($this->input->post('fname'))
                $_POST['contact_name'] = $_POST['account_name'] = $this->input->post('fname');

            $_POST['newsletter'] = $this->input->post('email_prefer');

            $this->profile->updateUserProfile('members', 'id');
        }

        $this->profile->updateUserProfile();
        $this->profile->updateUserProfile('accounts', 'account_id');
 
        $this->_loadUserProfile();
    }
}

/* End of file account.php */
/* Location: ./application/controllers/account.php */
