<?php 
include 'common.php';
include 'includes/header.php';

?>
         <div class="container">
         	<div class="wrapper"> 
			<!-- content-wrap starts here -->	
			<div id="content-wrap">  
					<div id="sidebar"></div>
			
					<div id="intro">
					<?php echo isset($Errors) ? $Errors : NULL; ?>	
						<?php if(isset($contentPage)) include "modules/$contentPage.php"; ?>			
									
			  		</div>			
										
			<!-- content-wrap ends here -->	
			</div>
		</div>
		</div>
<?php include 'includes/footer.php'?>
