<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Userprofile {
	var $table = 'users';
	var $logged_in = true;
	var $fields = array(
		'account_type',
		'email_address',
		'username',
		'contact_name',
		'default_account_id',
		'active',
		'comments',
		'date_created'
	);

	function get_user_profile($id=NULL)
	{
		return (object) $this->fields;
	}

//JSON Functions

	function get_zones($zoneid)
	{
		$CI = get_instance();
		$params = array();
		
		if($zoneid!=NULL) $params['zoneid']=$zoneid;
		$zones = $CI->adsmodel->get_zones($params);
		
		if($zones != false)
		{
			if ($zones->num_rows() > 0)
			{
			   foreach ($zones->result() as $row)
			   {
		      
			      if($zoneid!=NULL)
			      {
			      	foreach($row as $key=>$val)
			      		$result[$key] = $val;
			      }
			      else
					$result[$row->zoneid] = $row->zonename;

			   }
			} 
		}
		
		if(isset($result))
			echo json_encode($result);
		else
			echo json_encode(array('errors'=>'No zones found'));
	}

	function get_keywords($id=NULL){
		$CI = get_instance();
		if($id!=NULL){
			$CI->db->where('bannerid', $id);
			$banner = $CI->db->get('banners');
			
			if ($banner->num_rows() == 1)
			{
				$row = $banner->row();
				$keywords = explode('||', $row->keyword);
				
				foreach($keywords as $key)
					$result[trim(strtolower($key))] = trim($key);
			} 
		}
		else
		{
			$CI->db->where('show', 1);
			$CI->db->order_by('keyword');
			$keywords = $CI->db->get('keywords');
			
			if ($keywords->num_rows() > 0)
			{
			   foreach ($keywords->result() as $row)
			   {
			      $result[$row->keywordid] = $row->keyword;
			   }
			} 
		
		}
		
		if(isset($result))
			echo json_encode($result);
		else
			echo json_encode(array('errors'=>'No keyword found'));
	}

    function get_ads($bannerid=NULL, $campaign=NULL, $account_id=NULL)
    {
		$CI = get_instance();
		$params = array();
		
		if($bannerid!=NULL) $params['bannerid']=$bannerid;
		
		$banners = $CI->adsmodel->get_ad($params);
		
		if ($banners->num_rows() > 0)
		{
		   foreach ($banners->result() as $row)
		   {
		      
		      if($bannerid!=NULL)
		      {
		      	foreach($row as $key=>$val)
		      		$result[$key] = $val;
		      }
		      else
			      $result[$row->bannerid] = $row->description;
		   }
		} 
		
		if(isset($result))
			echo json_encode($result);
		else
			echo json_encode(array('errors'=>'No banner found'));
	}

    function get_campaigns($id=NULL)
    {
		$CI = get_instance();
		$campaigns = $CI->campaignmodel->get_campaigns();
		
		if($campaigns != false)
		{
			if ($campaigns->num_rows() > 0)
			{
			   foreach ($campaigns->result() as $row)
			   {
			      $result[$row->campaignid] = $row->campaignname;
			   }
			} 
		}
		
		if(isset($result))
			echo json_encode($result);
		else
			echo json_encode(array('errors'=>'No campaign found'));
	}
	
}

?>
