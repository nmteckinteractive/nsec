<?php
    function build_logo_section($type=NULL){
        $CI =& get_instance();

        if($CI->pageType != 'account')
        {
            if(is_logged_in())
            {
                $link = '
                    <div class="menu">
                        <a href="/account">My Account</a>
                        | <a href="/account/logout">Logout</a>
                    </div>
                ';
            } else {
                $link = '
                ';

            }

            $return = '<div id="top_links">'.$link.'</div>';

            return $return;
        }
    }

    function build_header_scripts($title=NULL, $section=NULL, $content=NULL){
        $CI =& get_instance();

        if ($content != NULL) {
            $title = $content->MetaTitle;
            $keywords = $content->MetaTags;
            $description = $content->MetaDesc;
        } else {
            $keywords = $description = NULL;
        }

        if($title != NULL) {
            $title .= ' | ';
        }

        $title .= $CI->config->item('SiteName');

        $return = <<<HTML
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>$title</title>
    <meta name="title" content="$title"/>
    <meta name="description" content="$description"/>
    <meta name="keywords" content="$keywords"/>
    <meta name="author" content=""/>
    <meta name="Copyright" content=""/>

    <link rel="stylesheet" href="{$CI->config->item('css_dir')}screen/style.css"/>
    <link rel="stylesheet" href="{$CI->config->item('css_dir')}modules.css"/>
    <link rel="stylesheet" href="{$CI->config->item('css_dir')}jquery.css"/>

    <script src="{$CI->config->item('js_dir')}head.js" type="text/javascript" ></script>

    <!--[if lt IE 9]>
        <script src="js/ie.js"></script>
        <link rel="stylesheet" href="css/ie.css"> 
    <![endif]-->
    <!--[if IE 9]>
        <link rel="stylesheet" href="css/ie9.css"> 
    <![endif]-->

HTML;

        if (!in_array($section, array('account', 'admin'))) {
            $return .= "\n<link rel=\"stylesheet\" href=\"{$CI->config->item('css_dir')}screen/page.css\"/>";
        } else {
            $return .= "\n<link rel=\"stylesheet\" href=\"{$CI->config->item('css_dir')}screen/admin.css\"/>";
        }


        return $return;
    }

    function getAnalyticsCode(){
        $CI =& get_instance();
        $return = '';

        if(strpos($_SERVER['HTTP_HOST'], 'www.') === true) {
            $return = <<<EOD
                <div style="display:none">
                    <script type="text/javascript">
                        <!--
                        var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
                        document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
                        // --></script>
                        <script type="text/javascript"><!--
                        try {
                        var pageTracker = _gat._getTracker("UA-2799369-12");
                        pageTracker._trackPageview();
                        } catch(err) {}
                        // -->
                    </script>
                </div>
EOD;
        }

        return $return;
    }

    function build_page_footer($section = null){
        $CI =& get_instance();

        $date = date('Y');
        $statsCode = getAnalyticsCode();

        $year = date('Y');

        $return = <<<HTML
            <div class="links">
                <a href="/login/" target="_top">Login</a>
                . <a href="/" target="_top">Home</a>
            </div>

            <div class="notice">
                &copy; {$year} MyDomain.com
                | All rights reserved.
            </div>

            <script src="{$CI->config->item('js_dir')}jquery.js" type="text/javascript" ></script>

HTML;

        if(in_array($section, array('account', 'admin', 'listings'))) {
            $CI =& get_instance();
            $return .= <<<HTML
                <script src="{$CI->config->item('js_dir')}account.js" type="text/javascript" ></script>
                <script type="text/javascript" >
                $(document).ready(function() {
                    menuWrapper = $('<div />');

                    $('#wrapper').accountFunctions({
                        menuContainer: '#header',
                        listWrapper: menuWrapper
                    });

                    if ($('#contentManagerForm').length > 0) {
                        $('#contentManagerForm').contentManager({

                        });
                    }
                });
                </script>

HTML;
        }

        if(isset($section) && in_array($section, array('edit_content'))) {
            $CI =& get_instance();
            $return .= <<<HTML

HTML;

        }

        $return .= <<<HTML
            <script src="{$CI->config->item('js_dir')}jquery.modules.js" type="text/javascript" ></script>
            <script src="{$CI->config->item('js_dir')}nmt.modules.js" type="text/javascript" ></script>

            <script type='text/html' id="loading_notice">Loading...</script>
            <script type='text/javascript'>
                        <!--
                        $(document).ready(function() {
                            hideLoadingPopup();
                        });
                        //-->
            </script>

                
HTML;

        $return .= "\n$statsCode";

        return $return;


    }
