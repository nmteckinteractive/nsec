<?php

function countUserListings($id = null, $active = true, $type = 'all')
{
    $CI = &get_instance();
    if ($id == null)
        $id = $CI->session->userdata('default_account_id');

    if ($type == 'pending') {
        $CI->db->where('Approved', 0);
    } elseif ($active)
        $CI->db->like('DateDeleted', '0000-00-00', 'after');
    
    $CI->db->where('UserID', $id);
    $listings = $CI->db->get('member_listings');

    return $listings->num_rows();
}

function getUserPayments($id=null) 
{
    $CI = &get_instance();
    
    if ($id != null) 
        $CI->db->where('UserID', $id);
    else
        $CI->db->where('UserID', $CI->session->userdata('default_account_id'));

    return $CI->db->get('member_payments');
}

function showVehicleRequestsList($id=null)
{
    $CI = &get_instance();

    if ($id != null) 
        $CI->db->where('ListingID', $id);
    else
        $CI->db->order_by('ListingID', 'desc');

    $listings = $CI->db->get('member_requests');

    $CI->load->library('table');
    $data = array(
        array('ID', 'Title', 'Added') 
    );

    $count = 0;
    foreach ($listings->result() as $listing) {
        $data[] = array(
            ++$count,
             '<b>' . $listing->Year . ' ' . $listing->Make . ' ' . $listing->Model . ' - ' . $listing->Trim . '</b>'
             . '<br />' . $listing->FirstName . ' ' . $listing->LastName
             . ($listing->hideEmail ? '' : '<br />' . $listing->Email),
            substr($listing->DateSaved, 0, 10)
        );
    }

    $tmpl = array('table_open' => '<table width="100%" class="site_requests" border="0" cellpadding="4" cellspacing="5">');
    $CI->table->set_template($tmpl);

    return $CI->table->generate($data) . '<hr />';
}

function showHomepageSpecialOffer() {
    return <<<HTML
    <div class="indent-block">
        <div class="txt-2">Special<br> Offer!</div>
        <span class="txt-1">iNeed the iPad.</span>
        <div><a href="/special-offer/" class="button">details</a></div>
    </div>
HTML;
}

function showListingsOnDashboard($id = null)
{
    $CI = &get_instance();
    $data['hasId'] = ($id == null) ? false : true;
    $data['account_id'] = $id;
    if ($id == null) $id = $CI->session->userdata('default_account_id');

    $CI->db->order_by('DateDeleted, DateSaved desc');

    $CI->db->where('UserID', $id);
    $data['listings'] = $CI->db->get('member_listings');

    $CI->load->library('table');

    $data['profile'] = new $CI->nsec_userprofile();

    $return = $CI->load->view('modules/listing/edit_row', $data, true)
        . ($CI->config->item('chargeForListings') ? $CI->load->view('modules/listing/payments', $data, true) : '');

    if ($data['hasId'] == false) {
        $return .= $CI->load->view('modules/forms/customer_support', null, true);
    }

    return $return;
}

function showNewListingForm($type=null, $listing = null)
{
    $CI = &get_instance();
    $return = null;

    $table = 'member_listings';
    if ($type != null) {
        $table = 'member_requests';
    }

    if ($type == null && !$CI->session->userdata('logged_in')) {
        $CI->session->set_userdata('redirect_url', 'sell');
        redirect('login');
    }

    if ($CI->input->post('saveListing') && !$CI->input->post('bpspchk')) {
        $postData = array(
            'Title' => $CI->input->post('title'),
            'Description' => $CI->input->post('description'),
            'Condition' => $CI->input->post('condition'),

            'FirstName' => $CI->input->post('first_name'),
            'LastName' => $CI->input->post('last_name'),
            'Location' => $CI->input->post('location'),
            'Email' => $CI->input->post('email'),
            'Phone' => $CI->input->post('phone'),
            'hideEmail' => $CI->input->post('hideEmail'),
            'hidePhone' => $CI->input->post('hidePhone'),
            'Price' => $CI->input->post('price'),
            'obo' => $CI->input->post('obo')
        );

        // Add category to post data
        if ($CI->input->post('customCategory')) {
            $CI->db->set(array(
                'categoryName'=> ucwords($CI->input->post('customCategory'))
            ));
            $CI->db->insert('category_types');
            $postData['Category'] = $CI->db->insert_id();
        } else {
            $postData['Category'] = $CI->input->post('category');
        }

        if ($CI->input->post('listingId')) {
            if ($_FILES['image1']['name'] != '') $postData['ListingImage'] = uploadListingImage('image1');
            if ($_FILES['image2']['name'] != '') $postData['ListingImage1'] = uploadListingImage('image2');
            if ($_FILES['image3']['name'] != '') $postData['ListingImage2'] = uploadListingImage('image3');
            if ($_FILES['image4']['name'] != '') $postData['ListingImage3'] = uploadListingImage('image4');
            if ($_FILES['image5']['name'] != '') $postData['ListingImage4'] = uploadListingImage('image5');
            if ($_FILES['image6']['name'] != '') $postData['ListingImage5'] = uploadListingImage('image6');
            if ($_FILES['image7']['name'] != '') $postData['ListingImage7'] = uploadListingImage('image7');
            if ($_FILES['image8']['name'] != '') $postData['ListingImage8'] = uploadListingImage('image8');
            if ($_FILES['image9']['name'] != '') $postData['ListingImage9'] = uploadListingImage('image9');
            if ($_FILES['image10']['name'] != '') $postData['ListingImage10'] = uploadListingImage('image10');

            $CI->db->where('ListingID', $CI->input->post('listingId'));
            $CI->db->set($postData);
            $CI->db->update($table);

            $listingId = $CI->input->post('listingId');
        } else {

            $postData['UserID'] = $CI->session->userdata('default_account_id');
            $postData['DateSaved'] = date('Y-m-d H:i:s');
            $postData['Approved'] = $CI->config->item('chargeForListings') ? 0 : 1;

            $postData['ListingImage'] = uploadListingImage('image1');
            $postData['ListingImage1'] = uploadListingImage('image2');
            $postData['ListingImage2'] = uploadListingImage('image3');
            $postData['ListingImage3'] = uploadListingImage('image4');
            $postData['ListingImage4'] = uploadListingImage('image5');
            $postData['ListingImage5'] = uploadListingImage('image6');
            $postData['ListingImage6'] = uploadListingImage('image7');
            $postData['ListingImage7'] = uploadListingImage('image8');
            $postData['ListingImage8'] = uploadListingImage('image9');
            $postData['ListingImage9'] = uploadListingImage('image10');

            $CI->db->set($postData);
            $CI->db->insert($table);

            $listingId = $CI->db->insert_id();
        }

        if ($type == null) {
            redirect('listings/edit/' . $listingId);
        } else {
            redirect('requests/thankyou');
        }

    } else {
        $return = getNewListingFormHTML($type, $listing);
    }
    return $return;
}

function getNewListingFormHTML($type = null, $listing = null)
{
    $CI = &get_instance();

    $listingid = $year = $make = $model = null;
    $location = $email = $phone = null;
    $mileage = $vin = $trim = null;
    $category = $condition = $firstName = $lastName = $email = null;
    $title = $price = $description = $images = null;

    $dropdown['description'] = null;
    $dropdown['shipping'] = null;
    $hidePhone = $hideEmail = $obo = 0;

    if ($listing != null && is_object($listing)) {
        // 10/10/2013 DTT: Prevent other users from editing other's listings
        if (!usertype('admin') && $listing->UserID != $CI->session->userdata('default_account_id'))
            redirect('account');

        $dropdown['categories'] = form_dropdown('category', array('' => 'Select Category'), $listing->Category, 'class="rounded"');
        $dropdown['condition'] = form_dropdown('condition', $CI->config->item('conditionTypes'), $listing->Condition, 'class="rounded"');

        $dropdown['featuresSelection'] = '';
        $currentFeatures = explode(',', $listing->Features);

        $listingid = $listing->ListingID;
        $category = $listing->Category;
        $condition = $listing->Condition;
        $year = $listing->Year;
        $location = addslashes($listing->Location);
        $hideEmail = $listing->hideEmail;
        $hidePhone = $listing->hidePhone;
        $phone = $listing->Phone;
        $email = $listing->Email;
        $title = addslashes($listing->Title);
        $images = '';
        $obo = $listing->obo;
        $price = $listing->Price;
        $firstName = addslashes($listing->FirstName);
        $lastName = addslashes($listing->LastName);

        $dropdown['description'] = $listing->Description;
        $dropdown['shipping'] = $listing->Shipping;
    } else {    
        $dropdown['categories'] = form_dropdown('category', array('' => 'Select Category'), null, 'class="rounded"');
        $dropdown['condition'] = form_dropdown('condition', $CI->config->item('conditionTypes'), null, 'class="rounded"');
    }

    $return = <<<HTML
    <script src="/-/js/jquery.createAdActions.js" type="text/javascript" ></script>
    <script type="text/javascript" language="javascript">
        $(document).ready(function(){
            $('#createad').createAdActions({
                listingId: '$listingid',
                categoryId: '$category',
                condition: '$condition',
                location: '$location',
                price: '$price',
                obo: $obo,
                title: '$title',
                firstName: '$firstName',
                lastName: '$lastName',
                email: '$email',
                phone: '$phone',
                hideEmail: $hideEmail,
                hidePhone: $hidePhone,
                images: '$images'
            });
        });
    </script>
HTML;

    if ($type == null) {
        $type = 'form';
    }

    $return .= $CI->load->view('modules/listing/' . $type, $dropdown, true);

    return $return;
}

function homepageListingTemplate() {
    return <<<HTML
    <div class="col-3">
        <div class="indent1">
            <h5><!--LISTINGTITLE--></h5>
            <div class="resultListingContainer">
            <a href="%%LISTINGLINK%%"><img class="img-indent" src="%%LISTINGLISTINGIMAGE%%" width="143" border="0" /></a><br /> 
            </div>
            <strong>in <!--LISTINGLOCATION--></strong><br /> 
            <strong>Price: <span><a href="%%LISTINGLINK%%"><!--LISTINGPRICE--></a></span></strong>
        </div>
    </div>
HTML;
}

function getRelatedListings($id, $category, $limit = 3) {
    $CI = &get_instance();

    $CI->db->limit(3);
    $CI->db->like('DateDeleted', '0000-00-00', 'after');
    $CI->db->order_by('DateSaved', 'desc');
    $CI->db->where('t.Approved', '1');
    $CI->db->where('t.Featured', '1');

    $Listings = $CI->db->get('member_listings t');

    $count = 0;
    $RELATED_ADS = NULL;
    $listingTemplate = homepageListingTemplate();

    if($Listings!=false && $Listings->num_rows() > 0)
    {           
        foreach($Listings->result() as $Listing)
        {
            $ListingDetail = $listingTemplate;
            $ListingDetail = str_replace(
                array("%%LISTINGTITLE%%", "<!--LISTINGTITLE-->"),
                $Listing->Title, 
                $ListingDetail
            );
            
            $ListingDetail = str_replace("%%LISTINGLINK%%", "/listings/details/$Listing->ListingID", $ListingDetail);

            if ($Listing->Price > 0)
                $ListingDetail = str_replace("<!--LISTINGPRICE-->", '$' . number_format($Listing->Price, 2), $ListingDetail);
            else
                $ListingDetail = str_replace("<!--LISTINGPRICE-->", 'N/A', $ListingDetail);

            if($Listing->ListingImage != '')
                $Listing->ListingImage = "/-/images/listings/$Listing->ListingImage";
            else
                $Listing->ListingImage = '/-/images/nologo.gif';
        
            foreach($Listing as $sKey=>$sVal)
            {
                $sKey = strtoupper($sKey);
                $ListingDetail = str_replace(array("%%LISTING$sKey%%", "<!--LISTING$sKey-->"), $sVal, $ListingDetail);
            }
            
            $RELATED_ADS .= $ListingDetail;
        }
    }
    else
        return '';

    return <<<HTML
    <div align="center">
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <!-- Findapitbull Footer Links -->
        <ins class="adsbygoogle"
             style="display:inline-block;width:468px;height:60px"
             data-ad-client="ca-pub-4124944144977417"
             data-ad-slot="0997729068"></ins>
        <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
    </div>

    <div class="col">
        <h2>Recent Listings</h2>
        <div class="container bg">
            <div class="container bg1">
                    {$RELATED_ADS}
            </div>
        </div>
    </div>
HTML;


}
    
function get_offerBox(){
    $CI =& get_instance();

    $baseUrl = base_url();

    return <<<EOD
    <div class="tablehead" id="checkAvailaibility">
        <a name="checkAvailaibility"></a>
        <h2>Contact Seller</h2>
        <form action="http://ilm.nmteck.net/ilm/?id=isetbahamas" method="post" class="ilmform" id="isetbahamas" onsubmit="return ValidateData(this);">
            <div id="formdiv0" class="formdiv">
                <div class="divfield div_name" id="name">
                    <label for="name" class="requiredfield formlabel" id="label_name">To: </label>
                    {$CI->currentListing->FirstName} {$CI->currentListing->LastName}
                </div>
                <div class="divfield div_email" id="email">
                    <div><label for="name" class="requiredfield formlabel" id="label_email">From: <span id="reqire_tag">*</span></label>
                    <input placeholder="Your Email Address" type="text" name="email" value="" id="email" maxlength="50" size="25"
                        onchange="if(this.value!='' &amp;&amp; !echeck(this.value, 'alert')){ alert('Invalid Email'); this.focus()}"></div>
                </div><br />
                <div class="divfield div_message" id="message">
                    <p>Hello!</p>
                    <p>I am interested in the {$CI->currentListing->Title} you have listed on iSetBahamas.com for \${$CI->currentListing->Price}.
                    I would like to know more about this vehicle and I can be reached by phone at 
                    <input placeholder="Enter your phone number" type="text" name="phonenumber" value="" id="phonenumber" maxlength="50" size="25">.</p>
                    <p>Thank you, <br /><input placeholder="Your Name" type="text" name="name" value="" id="name" maxlength="50" size="25"></p>
                </div>
                <div class="divfield div_comments" id="comments">
                    <label for="comments" class="formlabel" id="label_comments">Comments</label>
                    <div class="textarea"><textarea class="rounded" name="comments" cols="25" rows="4" id="comments"></textarea></div>
                </div>
            </div><br />
            <input type="hidden" name="Subject" value="iSetBahamas.com web inquiry regarding {$CI->currentListing->Title}">
            <input type="hidden" name="Listing" value="{$baseUrl}listings/details/{$CI->currentListing->ListingID}">
            <input type="hidden" name="form_id" value="140">
            <input type="hidden" name="ToEmail" value="{$CI->currentListing->Email}">
            <input type="hidden" name="formfieldgroup" value="54">
            <input type="hidden" name="client_id" value="12">
            <input type="hidden" name="requiredVars" value="name::Name|email::Email|phonenumber::Phone">
            <input type="submit" name="submit" value="Check Availability" id="formSubmitButton"><br />
            <sup><a href="/legal/privacy-policy">iSetBahamas.com Privacy Policy</a></sup>
        </form>

    </div>
EOD;
}

function getHomepageFeaturedListings($limit = 9, $divTag = 'col-1') {
    $CI = &get_instance();

    $CI->db->limit($limit);
    $CI->db->like('DateDeleted', '0000-00-00', 'after');
    $CI->db->order_by('DateSaved', 'desc');
    $CI->db->where('t.Approved', '1');
    //$CI->db->where('t.Featured', '1');

    $Listings = $CI->db->get('member_listings t');

    $count = 0;
    $RECENTLY_ADDED3 = $RECENTLY_ADDED2 = $RECENTLY_ADDED = NULL;
    $listingTemplate = homepageListingTemplate();

    if($Listings!=false && $Listings->num_rows() > 0)
    {           
        foreach($Listings->result() as $Listing)
        {
            $ListingDetail = $listingTemplate;
            $ListingDetail = str_replace(
                array("%%LISTINGTITLE%%", "<!--LISTINGTITLE-->"),
                $Listing->Title, 
                $ListingDetail
            );
            
            $ListingDetail = str_replace("%%LISTINGLINK%%", "/listings/details/$Listing->ListingID", $ListingDetail);

            if ($Listing->Price > 0)
                $ListingDetail = str_replace("<!--LISTINGPRICE-->", '$' . number_format($Listing->Price, 2), $ListingDetail);
            else
                $ListingDetail = str_replace("<!--LISTINGPRICE-->", 'More Info', $ListingDetail);

            if($Listing->ListingImage != '')
                $Listing->ListingImage = "/-/images/listings/$Listing->ListingImage";
            else
                $Listing->ListingImage = '/-/images/nologo.gif';
        
            foreach($Listing as $sKey=>$sVal)
            {
                $sKey = strtoupper($sKey);
                $ListingDetail = str_replace(array("%%LISTING$sKey%%", "<!--LISTING$sKey-->"), $sVal, $ListingDetail);
            }
            
            if ($count > 5) 
                $RECENTLY_ADDED3 .= $ListingDetail;
            elseif ($count > 2) 
                $RECENTLY_ADDED2 .= $ListingDetail;
            else
                $RECENTLY_ADDED .= $ListingDetail;

            $count++;
        }
    }

    return <<<HTML
    <div class="$divTag">
        <div class="tail">
            <div class="container bg">
                <div class="container bg1">
                    {$RECENTLY_ADDED}
                </div>
            </div>
        </div>
        <div class="tail">
            <div class="container bg">
                <div class="container bg1">
                    {$RECENTLY_ADDED2}
                </div>
            </div>
        </div>
        <div class="container bg">
            <div class="container bg1">
                    {$RECENTLY_ADDED3}
            </div>
        </div>
    </div>
HTML;
}

function showListingPaymentForm($listing_id =null,  $account_id = null) 
{
    $CI = &get_instance();
    $message = '';

    // Configuration options
    $config['stripe_key_test_public']         = 'pk_test_kIJ1wNu3nzwEqGWVdyR62o11';
    $config['stripe_key_test_secret']         = 'sk_test_EFdrTQS47enTOLvGe8C1bOzG';
    $config['stripe_key_live_public']         = 'pk_live_aiqMpc77g3y2MBUFEMQD3shE';
    $config['stripe_key_live_secret']         = 'sk_live_8ZzvmrtEib1YWwqYkJHeHNas';
    $config['stripe_test_mode']               = FALSE;
    $config['stripe_verify_ssl']              = FALSE;

    $key = $config['stripe_key_live_public'];
    if ($config['stripe_test_mode']) {
        $key = $config['stripe_key_test_public'];
    }

    $listingsCount = 1; //countUserListings($account_id, true, 'pending');
    $amount = 1500 * ($listingsCount == 0 ? 1 : $listingsCount);

    if (isset($_POST['stripeToken']) && isset($_POST['listingId']) && ($_POST['listingId'] == $listing_id)) {
        $stripeChargeResult = submitStripeCharge($config, $amount, $account_id);
        if ($stripeChargeResult !== true) {
            $message = '<div class="errors">' . $stripeChargeResult . '</div>';
        } else {
            $message = '<div class="success">Successful. <a href="/account/dashboard">Refresh Dashboard.</a></div>';
            return $message;
        }
    }

    return <<<HTML
    $message
    <form action="" method="POST">
      <input name="listingId" type="hidden" value="$listing_id" />
      <script
        src="https://checkout.stripe.com/checkout.js" class="stripe-button"
        data-key="$key"
        data-amount="1500"
        data-email="{$CI->session->userdata('email_address')}"
        data-name="iSetBahamas.Com"
        data-description="1 listing ($15.00)"
        data-image="/-/images/logo.gif">
      </script>
    </form>
HTML;
}

function submitStripeCharge($config, $amount, $account_id)
{
    $CI = &get_instance();
    $CI->load->library('stripe', $config);
    $token  = $_POST['stripeToken'];
    $listingID = isset($_POST['listingId']) ? $_POST['listingId'] : null;

    $customerID = getUserAccountInfo('m2m_password', $account_id, 'accounts');

    if ($customerID == null) {
        $customer = json_decode($CI->stripe->customer_create(
          $token,
          $CI->session->userdata('email_address')
        ));
        $customerID = $customer->id;

        $CI->db->set('m2m_password', $customerID);
        $CI->db->where('account_id', $account_id);
        $CI->db->update('accounts');
    }

    /*$charge = json_decode($CI->stripe->invoiceitem_create(
      $customer->id,
      1500,
      'usd'
    ));*/

    $charge = json_decode($CI->stripe->charge_customer(
      $amount,
      $customerID,
      $listingID
    ));

    if (isset($charge->error)) {
        $CI->db->set(array(
            'UserID' => $account_id,
            'Type' => 1,
            'Valid' => 0,
            'Processed' => 1,
            'Price' => 0,
            'Post' => $charge->error->type,
            'Response' => $charge->error->message,
            'DateTime' => date('Y-m-d H:i:s')
        ));
        $CI->db->insert('member_payments');
        return $charge->error->message;
    } else {
        $CI->db->set(array(
            'UserID' => $account_id,
            'Type' => 1,
            'Valid' => 1,
            'Processed' => 1,
            'Price' => ($charge->amount / 100),
            'Post' => $charge->description,
            'Response' => 'Secure Payment via Stripe.com',
            'DateTime' => date('Y-m-d H:i:s')
        ));
        $CI->db->insert('member_payments');

        $CI->db->set('Approved', 1);
        $CI->db->where('ListingID', $listingID);
        $CI->db->update('member_listings');

        return true;
    }

    

    //var_dump($charge);
}
    
function uploadListingImage($field)
{  
    $CI = &get_instance();
    $config['upload_path'] = "./-/images/listings/";
    $config['allowed_types'] = 'gif|jpg|png|jpeg';
    $config['max_size'] = '10000';
    $config['max_width']  = '4000';
    $config['max_height']  = '4000';
    $config['encrypt_name']    = TRUE;
        
    
    $CI->load->library('upload', $config);

    if ( ! $CI->upload->do_upload($field)) {
        $error = $CI->upload->display_errors();
        var_dump($error);
        //$CI->validation->error_string = $error;
        return '';
    } else {
        $upload_data = $CI->upload->data();

         ////[ THUMB IMAGE ]
        $config2['image_library'] = 'gd2';
        $config2['source_image'] = $CI->upload->upload_path . $CI->upload->file_name;
        $config2['new_image'] = './-/images/listings/thumbnails/';
        $config2['maintain_ratio'] = FALSE;
        $config2['create_thumb'] = TRUE;
        $config2['width'] = 250;
        $config2['height'] = 250;
        $CI->load->library('image_lib',$config2); 

        if ( !$CI->image_lib->resize()){
            $CI->session->set_flashdata('errors', $CI->image_lib->display_errors('', ''));   
        }

        $config['image_library'] = 'gd2';
        $config['source_image'] = $CI->upload->upload_path . $CI->upload->file_name;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = 1000;
        $config['height'] = 1000;
        $CI->load->library('image_lib',$config); 

        if ( !$CI->image_lib->resize()){
            $CI->session->set_flashdata('errors', $CI->image_lib->display_errors('', ''));   
        }

        return $upload_data['file_name'];        
    }
}

?>