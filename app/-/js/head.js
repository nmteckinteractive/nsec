/*! 242rentals 2017-10-23 */
function convertObjectToArray(object) {
    var newArray = [];
    for (var key in object) {
        newArray.push(object[key]);
    }
    return newArray;
}

var dayOfWeek = new Array();

dayOfWeek[0] = "Sunday";

dayOfWeek[1] = "Monday";

dayOfWeek[2] = "Tuesday";

dayOfWeek[3] = "Wednesday";

dayOfWeek[4] = "Thursday";

dayOfWeek[5] = "Friday";

dayOfWeek[6] = "Saturday";

function daysInMonth(iMonth, iYear) {
    return 32 - new Date(iYear, iMonth, 32).getDate();
}

function getMonday(d) {
    var day = d.getDay(), diff = d.getDate() - day + (day == 0 ? -6 : 1);
    // adjust when day is sunday
    return new Date(d.setDate(diff));
}

// this fixes an issue with the old method, ambiguous values
// with this test document.cookie.indexOf( name + "=" );
function Get_Cookie(check_name) {
    // first we'll split this cookie up into name/value pairs
    // note: document.cookie only returns name=value, not the other components
    var a_all_cookies = document.cookie.split(";");
    var a_temp_cookie = "";
    var cookie_name = "";
    var cookie_value = "";
    var b_cookie_found = false;
    // set boolean t/f default f
    for (i = 0; i < a_all_cookies.length; i++) {
        // now we'll split apart each name=value pair
        a_temp_cookie = a_all_cookies[i].split("=");
        // and trim left/right whitespace while we're at it
        cookie_name = a_temp_cookie[0].replace(/^\s+|\s+$/g, "");
        // if the extracted name matches passed check_name
        if (cookie_name == check_name) {
            b_cookie_found = true;
            // we need to handle case where cookie has no value but exists (no = sign, that is):
            if (a_temp_cookie.length > 1) {
                cookie_value = unescape(a_temp_cookie[1].replace(/^\s+|\s+$/g, ""));
            }
            // note that in cases where cookie is initialized but no value, null is returned
            return cookie_value;
            break;
        }
        a_temp_cookie = null;
        cookie_name = "";
    }
    if (!b_cookie_found) {
        return null;
    }
}

function formatNumber(num) {
    var n = num.toString();
    var nums = n.split(".");
    var newNum = "";
    if (nums.length > 1) {
        var dec = nums[1].substring(0, 2);
        newNum = nums[0] + "." + dec + "%";
    } else {
        newNum = num;
    }
    return newNum;
}

function CurrencyFormatted(amount) {
    var i = parseFloat(amount);
    if (isNaN(i)) {
        i = 0;
    }
    var minus = "";
    if (i < 0) {
        minus = "-";
    }
    i = Math.abs(i);
    i = parseInt((i + .005) * 100);
    i = i / 100;
    s = new String(i);
    if (s.indexOf(".") < 0) {
        s += ".00";
    }
    if (s.indexOf(".") == s.length - 2) {
        s += "0";
    }
    s = minus + s;
    return s;
}

function showLoadingPopup(force) {
    if (loading_attempt < loading_attempt_limit && (url.search(/dashboard/g) != -1 || url.search(/ads/g) != -1) || force == true) {
        if (loading_scripts == false || force == true) {
            $("#loading_notice").dialog({
                modal: true,
                height: 130,
                width: 100,
                autoOpen: true,
                buttons: {
                    Close: function() {
                        $(this).dialog("close");
                    }
                },
                position: [ "top", "center" ]
            });
            $(".ui-dialog-titlebar").hide();
            loading_scripts = true;
        }
        loading_attempt++;
    }
}

function popUp(URL) {
    day = new Date();
    id = day.getTime();
    eval("page" + id + " = window.open(URL, '" + id + "', 'toolbar=0, scrollbars=1, location=0, statusbar=1, menubar=0, resizable=0, width=300, height=250, left = 595, top = 350');");
}

function hideLoadingPopup() {
    if ($("#loading_notice").is(":visible")) {
        $("#loading_notice").dialog("close");
    }
}