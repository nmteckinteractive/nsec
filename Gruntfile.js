module.exports = function(grunt) {
	// ===============================================
	// Variables
	// ===============================================
	require('time-grunt')(grunt);
    var head_js_files = [
		'assets/js/helpers/convert_object_to_array.js',
		'assets/js/helpers/date_functions.js',
		'assets/js/helpers/get_cookie.js',
		'assets/js/helpers/number_formatting.js',
		'assets/js/helpers/pop_up.js'
    ];

    var jquery_files = [
		'assets/js/lib/jquery.js',
		'assets/js/lib/jquery.ui.js'

    ];

	var jquery_module_files = [
		'assets/js/nmt.ui.js',
    	'assets/js/lib/jquery.flow.js',
		'assets/js/lib/jquery.message.js',
		'assets/js/lib/jquery.validation.js',
	];

	var nmt_module_files = [
		'assets/js/helpers/nav_dropdown.js',
		'assets/js/modules/nmt/jquery.nmtGallery.js',
		'assets/js/modules/nmt/jquery.nmtGallerySlider.js',
		'assets/js/modules/nmt/jquery.nmtModal.js'
	];
	
	var account_function_files = [
		'assets/js/accountFunctions.js'
	];

	var ie_js_files = [
		'assets/js/lib/html5.js'
	];

	var target_array = [
		{
		  dest: 'app/-/js/head.js',
		  src: head_js_files
		},
		{
		  dest: 'app/-/js/jquery.js',
          src: jquery_files
        },
		{
		  dest: 'app/-/js/jquery.modules.js',
		  src: jquery_module_files
		},
		{
		  dest: 'app/-/js/nmt.modules.js',
		  src: nmt_module_files
		},
		{
		  dest: 'app/-/js/account.js',
		  src: account_function_files
		},
		{
		  dest: 'app/-/js/ie.js',
		  src: ie_js_files
		}
	];

  var target_array_to_watch = target_array.filter(function (target_group) {
	return !('watch' in target_group) || target_group.watch;
  });

  var npm_package = grunt.file.readJSON('package.json');

  var maven_settings_path = process.env.MAVEN_SETTINGS || '~/.m2/settings.xml';
  var static_assets_version = process.env.BUILD_ID || '';
  var deploymentType = process.env.DEPLOYMENT_TYPE || 'snapshots';
  var buildVersion = npm_package.version;
  if (deploymentType == 'snapshots') {
	  buildVersion = buildVersion + '-SNAPSHOT';
  }

  var all_jshint_files = [
		'assets/js/lib/*.js',
		'assets/js/templates/*.js',
		'assets/js/modules/**/*.js',
		'assets/js/helpers/*.js'
	];


var static_bundle_files = [];
target_array.forEach(function(file) {
	static_bundle_files.push( { src: file.dest } )
});

  // Project configuration.
  grunt.initConfig({
	pkg: npm_package,
		concurrent: {
			dev: {
				tasks: ['copy', 'sass','uglify:dev']
			},
			prod:{
				tasks: ['copy', 'sass', 'uglify:prod']
			}
		},
		sass: {
			options: {
				sourceMap: false,
				sourceComments: true,
				outputStyle: 'compressed',
				precision: 5
			},
			files: {
				expand: true,
				cwd: "assets/sass",
				src: ['screen/*.scss', '*.scss'],
				dest: "app/-/css",
				ext: ".css"
			}
		},
		uglify: {
			dev:{
				options: {
					mangle: false,
					beautify: true,
					compress: false,
					preserveComments: 'all',
					banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
				},
				files: target_array
			},
			prod:{
				options: {
					mangle: false,
					beautify: false,
					compress: {},
					preserveComments: false
				},
				files: target_array
			}
		},

		bump: {
			options: {
				files: ['package.json'],
				updateConfigs: [],
				commit: true,
				commitMessage: 'Release v%VERSION%',
				commitFiles: ['package.json'],
				createTag: true,
				tagName: 'v%VERSION%',
				tagMessage: 'Version %VERSION%',
				push: true,
				pushTo: 'origin',
				gitDescribeOptions: '--tags --always --abbrev=1 --dirty=-d'
			}
		},

		cssmin: {
		  target: {
		    files: [{
		      expand: true,
		      cwd: 'release/css',
		      src: ['*.css', '!*.min.css'],
		      dest: 'release/css',
		      ext: '.min.css'
		    }]
		  }
		},

		watch: {
			options: {
				debounceDelay: 100,
				interval: 5007
			},
			css: {
				files: [
					'assets/sass/**/*.scss',
					'assets/sass/**/*.scss',
					'assets/sass/**/**/*.scss',
					'assets/sass/**/**/**/*.scss'
				],
				tasks: ['sass']
			},
			scripts: {
				files: account_function_files.concat(head_js_files)
					.concat(nmt_module_files)
					.concat(jquery_files)
					.concat(jquery_module_files)
					.concat(account_function_files)
					.concat(ie_js_files),
				tasks: ['uglify:dev', 'jshint'],
				options: { livereload: true }
			},
			livereload: {
				files: ['app/-/css/*'],
				options: { livereload: true }
			},
			options: {
				nospawn : true
			}
		},

		jshint: {
			options: {
				force:true,
				bitwise: false,
				"-W099": true,
				browser: true,
				curly: true,
				elision: true,
				eqeqeq: true,
				eqnull: true,
				forin: true,
				immed: true,
				noarg: true,
				noempty: true,
				nonbsp: true,
				strict: false,
				undef: true,
				unused: true,
				globals: {
					jQuery: true
				}
			},
			all: all_jshint_files
		},

		cssmin: {
			target: {
				files: [{
					expand: true,
					cwd: 'app/-/css',
					src: ['*.css', '**/*.css', '!*.min.css'],
					dest: 'app/-/css',
					ext: '.min.css'
				}]
			}
		},

		// Take a look at: https://www.npmjs.org/package/grunt-maven-tasks, it apparently handles doing releases via git,
		// though I think the verion number will end up being a hash, so it may not be useful.
		shell: {
			echo_version: {
				command: 'echo "##teamcity[buildNumber \''+ buildVersion + '\']\"'
			}
		},

		copy: {
			main: {
				expand: true,
				cwd: 'assets/js/tiny_mce/',
				src: '**',
				dest: 'app/-/js/tiny_mce/'
			},
		},

		scsslint: {
			allFiles: [
				'assets/sass/*.scss',
				'assets/sass/**/*.scss',
				'assets/sass/**/**/*.scss',
				'assets/sass/**/**/**/*.scss'
			],
			options: {
				config: 'scss-lint.yml',
				force: true
			}
		},
	});

	grunt.event.on('watch', function(action, filepath) {
		filepath = filepath.replace(/\\/g, '/'); //replace for windows filepaths
		
		if(filepath.indexOf('.scss') === filepath.length - 5) { // SASS update
			grunt.config('scsslint.allFiles', [filepath]);
		} else { // JS update
			var target = null,
				updated_targets = [];

			if(filepath.indexOf('.html') === filepath.length - 5) { // it ends with .html, it generated templates, use that instead
				filepath = 'app/-/js/templates.js';
			}

			target_array_to_watch.forEach(function (target_group) {
				if(target_group.src.indexOf(filepath) > -1 || target_group.src.indexOf(filepath.substring(0, filepath.lastIndexOf('/') + 1) + '*.js') > -1) {
					updated_targets.push(target_group);
				}
			});

			if(!updated_targets.length) {
				// reset... previous targeted updateds may have overwrote these values
				grunt.config('uglify.dev.files', target_array_to_watch);
				grunt.config('jshint.all', all_jshint_files);
			} else {
				grunt.config('uglify.dev.files', updated_targets);
				grunt.config('jshint.all', [ filepath ]);            
			}
		}
	});

	// ===============================================
	// Tasks
	// ===============================================

	// Load the plugin that provides the "uglify" task.
	grunt.loadNpmTasks('grunt-contrib-uglify');

	//trying grunt-sass to improve sass compile time
	grunt.loadNpmTasks("grunt-sass");
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-concurrent');
	grunt.loadNpmTasks('grunt-sync');

	// Run Watch
	grunt.loadNpmTasks('grunt-contrib-watch');

	// Lint js
	grunt.loadNpmTasks('grunt-contrib-jshint');

	// Lint css
	grunt.loadNpmTasks('grunt-scss-lint');

	// Default task(s)
	var default_task = [
		'concurrent:dev',
		'watch'
	];
	var prod_task = [
		'concurrent:prod',
		'cssmin'
	];
	grunt.registerTask('default', default_task);
	grunt.registerTask('build', ['concurrent:dev']);
	grunt.registerTask('lint', ['jshint', 'scsslint']);
	grunt.registerTask('prod', prod_task);
	grunt.registerTask('release', ['shell:echo_version'].concat(prod_task));

};