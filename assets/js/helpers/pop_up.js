function showLoadingPopup(force){
    if(((loading_attempt < loading_attempt_limit)
        && ((url.search(/dashboard/g) != -1)
        || (url.search(/ads/g) != -1)))
        || (force == true))
    {
        if((loading_scripts == false) || (force == true))
        {
            $('#loading_notice').dialog({
                modal: true,
                height: 130,
                width: 100,
                autoOpen: true,
                buttons: {
                    "Close": function(){
                        $(this).dialog("close");
                    }
                },
                position: ['top', 'center']
            });
            $(".ui-dialog-titlebar").hide();
               loading_scripts = true;
        }

        loading_attempt++;
    }

}

function popUp(URL) {
    day = new Date();
    id = day.getTime();
    eval(
        "page"
        + id + " = window.open(URL, '"
        + id + "', 'toolbar=0, scrollbars=1, location=0, statusbar=1, menubar=0, resizable=0, width=300, height=250, left = 595, top = 350');"
    );
}

function hideLoadingPopup(){
    if ($('#loading_notice').is(':visible')) {
        $('#loading_notice').dialog('close');
    }
}