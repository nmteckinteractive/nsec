jQuery(document).ready(function($) {
    function animateDropDown(obj) {
        var $this = $(obj);

        $('.sublinks').stop(false, true).hide();
        $('.subdroplinks').stop(false, true).hide();

        var submenu = $this.next();

        submenu.closest('li').css({position: 'relative'});

        submenu.css({
            position:'absolute',
            top: ($this.offset().top + $this.height()) + 'px',
            left: $this.offset().left + 'px',
            zIndex: 999
        });

        submenu.stop().slideDown(300);

        submenu.mouseleave(function(){
            $(this).slideUp(300);
        });

        $this.parent().mouseleave(function(){
            submenu.slideUp(300);
        });

    }

    $('.dropdown').mouseenter(function(){
        animateDropDown(this);
    });

    $('.dropdown').on('mouseenter', function(){
        animateDropDown(this);
    });

    $('.subdropdown').mouseenter(function(){
        $('.subdroplinks').stop(false, true).hide();

        var subdropmenu = $(this).next();

        subdropmenu.css({
            position:'absolute',
            top: '0px',
            left: '150px',
            zIndex:1000
        });

        subdropmenu.stop().slideDown(300);

        subdropmenu.mouseleave(function(){
            $(this).slideUp(300);
        });
    });
});