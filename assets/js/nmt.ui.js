$(document).ready(function() {
    // Remove no-js tag if javascript is enabled
    $('html').removeClass('no-js');
    $( '.datepicker' ).datepicker({
        dateFormat: 'yy-mm-dd',
        numberOfMonths: 3,
        showButtonPanel: true
    });

    $.fn.hasAttr = function (attr) {
        for (var i = 0; i < this[0].attributes.length; i++) {
            if (this[0].attributes[i].nodeName == attr) {return true}
        }
       return false;
    };
});